FROM quay.io/centos/centos:stream9

RUN dnf update dnf -y

RUN dnf install epel-release -y

RUN dnf install dnf-plugins-core -y
#RUN dnf config-manager --set-enabled powertools  # el8
RUN dnf config-manager --set-enabled crb         # el9

RUN dnf install git rpm-build -y

RUN git clone https://github.com/open-iscsi/tcmu-runner.git /tmp/tcmu-runner
RUN cd /tmp/tcmu-runner && git checkout v1.6.0

RUN dnf -y install centos-release-gluster9
#RUN sed -i -e "s/enabled=1/enabled=0/g" /etc/yum.repos.d/CentOS-Gluster-9.repo
RUN dnf --enablerepo=centos-gluster9 -y install glusterfs-server
RUN dnf install glusterfs glusterfs-api

WORKDIR /tmp/tcmu-runner

RUN ./extra/install_dep.sh 

RUN cmake . --debug-output
RUN make
RUN make install
RUN cd ./extra && ./make_runnerrpms.sh