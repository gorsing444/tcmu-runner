#!/bin/sh
podman rm tcmurunner_rpms
podman build --pull --rm -f "Dockerfile" -t tcmurunner:stream9 "."

podman run --name tcmurunner_rpms tcmurunner:stream9

podman cp tcmurunner_rpms:/tmp/tcmu-runner/extra/rpmbuild .

